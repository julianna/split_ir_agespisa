/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ir_agespisa;

import java.io.IOException;
import java.util.List;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripper;

/**
 * @author julianna Carrega PDF e quebra as páginas Em seguida ler o conteúdo do
 * PDF e busca pelo CPF O arquivo será salvo no seguinte padrão: CPF.pdf
 */
public class IR_Agespisa {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, COSVisitorException {
        int startPage = 0;
        int endPage = Integer.MAX_VALUE;
        String textoExtraido = null;
        String nameFile = null;
        String arquivoCarregar = "/home/julianna/Informe_Rendimentos_2019.pdf";
        String destinoArquivo = "/home/julianna/IR/";

        PDDocument doc = new PDDocument();
        doc = PDDocument.load(arquivoCarregar);


        List pages = doc.getDocumentCatalog().getAllPages();

        for (int i = startPage; i <= endPage && i < pages.size(); i++) {
            PDDocument docSplit = new PDDocument();
            PDPage page = (PDPage) pages.get(i);
            docSplit.addPage(page);
            PDFTextStripper stp = new PDFTextStripper();
            textoExtraido = stp.getText(docSplit);
            nameFile = cpfColaborador(textoExtraido.trim(), i);
            docSplit.save(destinoArquivo + nameFile + ".pdf");
        }

        doc.close();

    }

    private static String imprimeCPF(String CPF) {
        return CPF.replace(".", "").replace("-", "").replace(" ", "");
    }

    private static String cpfColaborador(String textoExtraido, int j) {
        String[] textoQuebrado = textoExtraido.split("C.P.F.:");
        if (textoQuebrado.length > 1) {
            String[] cpf = textoQuebrado[0].split("\n");
            return imprimeCPF(cpf[4].substring(0, 14));
        }else{
            System.out.print("Página com erro"+j);
            return "Página com erro"+j;
        }
    }
}
